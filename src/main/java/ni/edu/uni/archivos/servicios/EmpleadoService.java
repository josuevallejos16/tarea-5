/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.archivos.servicios;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.Scanner;
import ni.edu.uni.archivos.implementation.EmpleadoDaoImplement;
import ni.edu.uni.archivos.main.ArchivosDatosApp;
import ni.edu.uni.archivos.model.EmpleadoModel;
import ni.edu.uni.archivos.pojo.Empleado;

/**
 *
 * @author yasser.membreno
 */
public class EmpleadoService implements IServices<Empleado>{
    private Scanner scan;
    private EmpleadoDaoImplement eDao;
    private EmpleadoModel eModel;

    public EmpleadoService(Scanner scan) {
        this.scan = scan;
        eDao = new EmpleadoDaoImplement();                
    }
    
    
    @Override
    public void create() throws IOException {
        int id, cod;
        String cedula, nombres, apellidos, inss, fecha;
        double salario;
        
        System.out.println("id: ");
        id = scan.nextInt();
        
        System.out.println("Codigo empleado: ");
        cod = scan.nextInt();
        
        scan.nextLine();
        
        System.out.println("Cedula: ");
        cedula = scan.nextLine();
        
        System.out.println("Nombre: ");
        nombres = scan.nextLine();
        
        System.out.println("Apellidos: ");
        apellidos = scan.nextLine();
        
        System.out.println("INSS: ");
        inss = scan.nextLine();
        
        System.out.println("Fecha de contratacion [dd/mm/yyyy]");
        fecha = scan.next();
        
        System.out.println("Salario: ");
        salario = scan.nextDouble();
        
        Empleado e = new Empleado(id, cod, cedula, nombres, apellidos, inss,
                LocalDate.parse(fecha,DateTimeFormatter.ofPattern("dd/M/yyyy") ), salario);
        
        eDao.create(e);
        System.out.println("El empleado se guardo satisfactoriamente!");
        
    }

    @Override
    public int update() throws IOException {
        int value = -1, id = -1;

        findAll();

        System.out.println("\nIngrese el Id del empleado por favor: ");
        id = scan.nextInt();

        Optional<Empleado> ollEmpleado = Optional.ofNullable(eDao.findById(id));

        if (!ollEmpleado.isPresent()){
            System.out.println("\n\t-> No existe un usuario con el ID escrito\n");
            return -1;
        }

        Empleado newEmpleado = new Empleado();
        newEmpleado.setId(ollEmpleado.get().getId());

        System.out.println("Codigo empleado: ");
        newEmpleado.setCodEmpleado(scan.nextInt());

        System.out.println("Cedula: ");
        newEmpleado.setCedula(scan.next());

        System.out.println("Nombre: ");
        newEmpleado.setNombres(scan.next());

        System.out.println("Apellidos: ");
        newEmpleado.setApellidos(scan.next());

        System.out.println("INSS: ");
        newEmpleado.setInss(scan.next());

        System.out.println("Fecha de contratacion [dd/mm/yyyy]");
        newEmpleado.setFechaContratacion(LocalDate.parse(scan.next(),DateTimeFormatter.ofPattern("dd/M/yyyy") ));

        System.out.println("Salario: ");
        newEmpleado.setSalario(scan.nextDouble());

        value = eDao.update(newEmpleado);

        String msg = value > 0 ? "\n\t-> Se ha actualizado el registro\n" : "\n\t->No se ha podido actualizar.\n";
        System.out.println(msg);

        return value;
    }

    @Override
    public boolean delete() throws IOException {
        int id = -1;
        boolean flag;
        findAll();

        System.out.print("\nIngrese el Id del empleado por favor: ");
        id = scan.nextInt();

        Optional<Empleado> ollEmpleado = Optional.ofNullable(eDao.findById(id));

        if (!ollEmpleado.isPresent()){
            System.out.println("\n\t-> No existe un usuario con el ID escrito\n");
            return false;
        }

        flag = eDao.delete(ollEmpleado.get());

        String msg = flag ? "\n\t-> Se ha eliminado el registro\n" : "\n\t->No se ha podido eliminar.\n";
        System.out.println(msg);

        return flag;
    }

    @Override
    public void findAll() throws IOException {
        printHeader();
        for(Empleado e : eDao.findAll()){
            ArchivosDatosApp.print(e);
        }
        
    }
    
    private void printHeader(){
        System.out.format("%5s %5s %20s %20s %20s %10s %25s %5s\n", 
                "Id","Codigo","Cedula","Nombres","Apellidos","Inss","Fecha de contratacion","Salario");
    }
    
    public void findByFechaContratacion() throws IOException{
        String fecha;
        eModel = new EmpleadoModel();
        System.out.println("Fecha de contratacion [dd/mm/yyyy]");
        fecha = scan.next();
        
        eModel.setEmpleados(eDao.findByFechaContratacion(LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/M/yyyy"))));
        if(eModel.getEmpleados() == null){
            System.out.println("No se encontraron Empleados en ese rango de fecha!");
            return;
        }
        printHeader();
        for(Empleado e : eModel.getEmpleados()){
            ArchivosDatosApp.print(e);
        }
    }
    
    public void findByRangoFecha() throws IOException{
        String fechaInicial, fechaFinal;        
        eModel = new EmpleadoModel();
        
        System.out.println("Fecha inicial [dd/mm/yyyy]");
        fechaInicial = scan.next();
        
        System.out.println("Fecha final [dd/mm/yyyy]");
        fechaFinal = scan.next();
        
        eModel.setEmpleados(eDao.findByRangoFecha(
                LocalDate.parse(fechaInicial, DateTimeFormatter.ofPattern("dd/M/yyyy")), 
                LocalDate.parse(fechaFinal, DateTimeFormatter.ofPattern("dd/M/yyyy"))
        ));
        if(eModel.getEmpleados() == null){
            System.out.println("No se encontraron Empleados en ese rango de fecha!");
            return;
        }
        printHeader();
        for(Empleado e : eModel.getEmpleados()){
            ArchivosDatosApp.print(e);
        }
    }
    
    //Tarea#5
    public void findByInss()throws IOException{
        String valorInss;
        Empleado e = null;

        System.out.println("Valor del Inss: ");
        valorInss = scan.nextLine();

        e = eDao.findByInss(valorInss);

        if(e == null){
            System.out.println("No se ha encontrado un empleado con ese valor de Inss");
        }else{
            printHeader();
            ArchivosDatosApp.print(e);
        }

        
    }
    
   
}
